#!/bin/bash

./clean.sh

mkdir cmake-build && cd $_

echo "# reload"
# 
#  -DBoost_NO_SYSTEM_PATHS=TRUE -DBOOST_ROOT=/mnt/e/opt/usr/lib/boost/boost_1_70
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=./rootfs -G "CodeBlocks - Unix Makefiles" ../

echo "# build"
# -- -j 2
cmake --build . --target install

echo "# strip"
strip -sv rootfs/bin/circle
# strip -sv rootfs/bin/circle_test
# strip -sv rootfs/lib/*