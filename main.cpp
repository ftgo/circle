/***********************************************************************************************************************
 Task              : Circle CVI Interview
 Assignment        : Draw a circle from the command line
 Reference         : https://cboard.cprogramming.com/cplusplus-programming/84704-drawing-circle-into-bitmap-file.html
                     https://stackoverflow.com/questions/1201200/fast-algorithm-for-drawing-filled-circles
 Author            : Felipe Toledo G Oliveira <felipetgoliveira@gmail.com>
 Exertion (sRPE)   : Medium
 Analysis          : Needs image processing knowledge. I could not create a .img file with details, but a bmp from the
                     references described above. ps.: no memory leak - checked with Valgrind.
 Revision History  :
 Date           Author      Revision (Date in YYYYMMDD format)
 20211007       ftgo        initial creation
 **********************************************************************************************************************/
#include<string>
#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <sstream>
#include <iterator>

using namespace std;

// see - https://cboard.cprogramming.com/cplusplus-programming/84704-drawing-circle-into-bitmap-file.html
class BMP {
private:
    const char BLACK = 255;

    string name_; // file name
    int width_, height_;


    unsigned char header[1078];
    unsigned char **data_;

    // returns a valid filename. If it exists, postfixes it
    static string get_name(const string &str) {
        string str2 = str;
        ifstream z = ifstream(str2);
        int j = 0;
        while (z.good()) {
            // FIXME throws an error if the file has no extension
            str2 = str.substr(0, str.find('.')) + "_" + to_string(j++) + str.substr(str.find('.'));
            z = ifstream(str2);
        }
        return str2;
    }

public:
    BMP(string name, int width, int height) : name_(move(name)), width_(640), height_(480) {
        data_ = new unsigned char *[width_];
        for (int i = 0; i < width_; ++i) {
            data_[i] = new unsigned char[height_];
        }

        // FIXME find out how to define the BMP image size. It should be set on headers

        // BMP mandatory headers
        // see - http://www.ece.ualberta.ca/~elliott/ee552/studentAppNotes/2003_w/misc/bmp_file_format/bmp_file_format.htm

        for (int i = 0; i < 1078; i++)
            header[i] = 0;

        header[0] = 'B';
        header[1] = 'M';
        unsigned char *p = &header[2];
        *(int *) (p) = 0x4b436;
        p = &header[6];
        *(int *) (p) = 0x0;
        p = &header[10];
        *(int *) (p) = 0x436;
        p = &header[14];
        *(int *) (p) = 0x28;
        p = &header[18];
        *(int *) (p) = 0x280;
        p = &header[22];
        *(int *) (p) = 0x1e0;
        p = &header[26];
        *(unsigned short *) (p) = 0x1;
        p = &header[28];
        *(unsigned short *) (p) = 0x8;
        p = &header[30];
        *(int *) (p) = 0x0;
        p = &header[34];
        *(int *) (p) = 0x4b000;
        p = &header[38];
        *(int *) (p) = 0x0;
        p = &header[42];
        *(int *) (p) = 0x0;
        p = &header[46];
        *(int *) (p) = 0x0;
        p = &header[50];
        *(int *) (p) = 0x0;

        p = &header[54];
        *(int *) (p) = 0x0;
        p = &header[58];
        *(int *) (p) = 0x800000;
        p = &header[62];
        *(int *) (p) = 0x8000;
        p = &header[66];
        *(int *) (p) = 0x808000;
        p = &header[70];
        *(int *) (p) = 0x80;
        p = &header[74];
        *(int *) (p) = 0x800080;
        p = &header[78];
        *(int *) (p) = 0x8080;
        p = &header[82];
        *(int *) (p) = 0x808080;
        p = &header[86];
        *(int *) (p) = 0xC0C0C0;
        p = &header[90];
        *(int *) (p) = 0xFF00000;
        p = &header[94];
        *(int *) (p) = 0xFF00;
        p = &header[98];
        *(int *) (p) = 0xFFFF00;
        p = &header[102];
        *(int *) (p) = 0xFF;
        p = &header[106];
        *(int *) (p) = 0xFF00FF;
        p = &header[110];
        *(int *) (p) = 0xFFFF;
        p = &header[114];
        *(int *) (p) = 0xFFFFFF;


        // initialize data

        for (int i = 0; i < width_; i++)
            for (int j = 0; j < height_; j++)
                data_[i][j] = 15;
    }

    virtual ~BMP() {
        for (int i = 0; i < width_; ++i) {
            delete[] data_[i];
        }
        delete[] data_;
    }

    // algorithm to draw a circle to the BMP
    // see - https://stackoverflow.com/questions/1201200/fast-algorithm-for-drawing-filled-circles
    // see - https://stackoverflow.com/a/14976268/1060392
    void circle(int radius, int x0, int y0) {
        int x = radius;
        int y = 0;
        int xChange = 1 - (radius << 1);
        int yChange = 0;
        int radiusError = 0;

        while (x >= y) {
            for (int i = x0 - x; i <= x0 + x; i++) {
                data_[i][y0 + y] = BLACK;
                data_[i][y0 - y] = BLACK;
            }
            for (int i = x0 - y; i <= x0 + y; i++) {
                data_[i][y0 + x] = BLACK;
                data_[i][y0 - x] = BLACK;
            }

            y++;
            radiusError += yChange;
            yChange += 2;
            if (((radiusError << 1) + xChange) > 0) {
                x--;
                radiusError += xChange;
                xChange += 2;
            }
        }
    }

    // save the file
    void save() {
        // first, check the need to postfix the filename
        string str2 = get_name(this->name_);
        ofstream f(str2.c_str());

        // write BMP headers and data

        for (int i = 0; i < 1078; i++)
            f << header[i];

        for (int i = 0; i < height_; i++)
            for (int j = 0; j < width_; j++)
                f << data_[j][i];

        f.close();
    }


};

class Program {
public:
    BMP *bmp = nullptr;

    virtual ~Program() {
        delete this->bmp;
    }

    bool initialized() const {
        return this->bmp != nullptr;
    }

    void start(string name, int width, int height) {
        if (bmp != nullptr)
            this->bmp->save();

        // FIXME width and height is not working
        this->bmp = new BMP(move(name), width, height);
    }

    void circle(int radius, int centerX, int centerY) {
        if (this->bmp == nullptr)
            return;

        this->bmp->circle(radius, centerX, centerY);

        this->bmp->save();
        delete bmp;
        bmp = nullptr;
    }

    int exit() const {
        if (this->bmp != nullptr)
            this->bmp->save();

        return 0;
    }
};

/*
 * example:
 *
 * start a.bmp 1000 1000
 * circle 30 60 60
 * start a.bmp 1000 1000
 * circle 50 100 100
 * exit
 **/
int main() {
    Program program;

    // Process the standard input commands

    while (true) {
        cout << "circle> ";

        string line;
        getline(cin, line);
        istringstream iss(line);
        vector<string> results(istream_iterator<string>{iss},
                               istream_iterator<string>());

        if (results.empty())
            continue;

        // FIXME needs better validation

        string cmd = results[0];

        if (cmd == "start") {
            if (results.size() != 4) {
                cout << "usage: start <filename>.bmp <width> <height>" << endl;
                continue;
            }

            program.start(results[1], stoi(results[2]), stoi(results[3]));

        } else if (cmd == "circle") {
            if (!program.initialized()) {
                cout << "image not initialized. call 'start' first." << endl;
                continue;
            }

            if (results.size() != 4) {
                cout << "usage: start circle <radius> <height>" << endl;
                continue;
            }

            program.circle(stoi(results[1]), stoi(results[2]), stoi(results[3]));

        } else if (cmd == "exit") {
            return program.exit();
        } else {
            cout << "command not recognized (to finish, type 'exit')" << endl;
        }
    }
}
